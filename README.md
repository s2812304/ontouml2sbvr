# OntoUML Translator

## Overview
The OntoUML Translator is a prototype tool developed by Tom Nieuwland for his Bachelor Thesis at the University of Twente. 
This tool translates OntoUML models serialized in OntoUML Vocabulary into natural language specifications and speech. 
The aim of the thesis and project were to enhance the accesibility of OntoUML.

## Features
- Translation of OntoUML Vocabulary in to SBVR entries
- Translation of SBVR entries into Natural Language specification
- Transformation of Natural Language specification into speech

## Installation
To install and run the OntoUML Translator, follow these steps:
1. Clone the repository:
    ```bash
    git clone https://gitlab.utwente.nl/s2812304/ontouml2sbvr.git
    ```
2. Navigate to the project directory:
    ```bash
    cd ontouml2sbvr
    ```
3. Install the necessary dependencies (assuming you have Python and pip installed):
    ```bash
    pip install -r requirements.txt
    ```

## Usage
1. Add your OntoUML models, serialized in OntoUML Vocabulary, to the directory 'Resources/OntoUML'

2. Move to the 'src' directory using:
    ```bash
    cd src
    ```

2. Use the OntoUML Translator by running the main script:
    ```bash
    python main.py
    ```

3. All outputs: SBVR entries, Natural Language specification and Speech can be found in 'Resources/SBVR', 'Resources/NL', and 'Resources/Speech' respectively. 

Multiple models can be translated in the same run, all files in the directory will be translated and create respective output files. 

The tool can also perform a single translation instead of running all translations back-to-back.
- To run the OntoUML Vocabulary to SBVR specification translation, use 'python ontouml2sbvr.py', from the directory 'src/OntoUML2SBVR'
- To run the SBVR specification to Natural Language specification translation, use 'python sbvr2nl.py', from the directory 'src/SBVR2NL'
- To run the Natural Lanauge specification to Speech translation, use 'python nl2speech.py', from the directory 'src/NL2Speech'

Note that to run the any step, a correct input file has to be present in the Resource folder from the starting type. 

## Acknowledgment
This project was developed as part of a Bachelor Thesis at the University of Twente. Special thanks to Pedro Paulo Favato Barcelos for his supervision, advice and guidance during the research project.

## License
This project is licensed under the Apache License 2.0. See the LICENSE file for details.

## Contributors
The tool was produced by:
-  [Tom Nieuwland](https://orcid.org/0009-0009-7564-2175) [[mail]](t.nieuwland@student.utwente.com)