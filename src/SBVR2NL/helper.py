"""
Module Name: helper.py

This module contains various functions for to aid the sbvr2nl module.
"""

from itertools import combinations
from collections import defaultdict
import re

vowel_sounds = ["A", "E", "I", "O", "U"]


def list_concepts(concepts):
    """
    Generate a string listing the given concepts.

    Args:
        concepts (list): List of concepts to be listed.

    Returns:
        str: A formatted string listing the concepts.
    """

    result = f"Every {concepts[0]}"
    for i in range(1, len(concepts)-1):
        result += f",{concepts[i]}"
    result += f", or {concepts[-1]}"

    return result


def reduce_concepts(sentences):
    """
    Combine sentences with the same identifier and target.

    Args:
        sentences (list): List of sentences to be processed.

    Returns:
        dict: Dictionary of old sentences mapped to changed sentences.
    """

    changed = dict()
    combined_concepts = defaultdict(list)
    for sentence, identifier, target in sentences:
        combined_concepts[(identifier, target)].append(sentence)

    for (identifier, _), sentence in combined_concepts.items():
        if not len(sentence) == 1:
            concepts = []
            for line in sentence:
                concepts.append(line[2:].split(" " + identifier)[0])
            for s in sentence:
                changed[s] = f"{list_concepts(concepts)} {identifier}{sentence[0].split(' ' + identifier)[1]}"

    return changed


def reduce_targets(sentences):
    """
    Combine sentences with the same concept and identifier.

    Args:
        sentences (list): List of sentences to be processed.

    Returns:
        dict: Dictionary of old sentences mapped to changed sentences.
    """

    changed = dict()
    combined_targets = defaultdict(list)
    for sentence, concept, identifier in sentences:
        combined_targets[(concept, identifier)].append(sentence)

    for (_, identifier), sentence in combined_targets.items():
        if not len(sentence) == 1:
            combined_sentence = sentence[0].split(".")[0]
            for i in range(1, len(sentence) - 1):
                combined_sentence += f",{sentence[i].split(identifier)[1].split('.')[0]}"
            combined_sentence += f", and{sentence[-1].split(identifier)[1].split('.')[0]}. "
            for s in sentence:
                changed[s] = combined_sentence

    return changed


def handle_general(rule, concept):
    """
    Generate a general sentence based on the given rule and concept.

    Args:
        rule (str): The rule to be processed.
        concept (str): The concept that the rule is about.

    Returns:
        str: A formatted sentence based on the rule and concept.
    """

    if "exactly one" in rule:
        target = rule.split("exactly one")[1]
        if target[1] in vowel_sounds:
            return f"Every {concept} is either an{target}. "
        else:
            return f"Every {concept} is either a{target}. "
    elif "at least one" in rule:
        target = rule.split("at least one")[1]
        return f"Every {concept} is at least one{target}, but can also be multiple at the same time. "
    elif "at most one" in rule:
        target = rule.split("at most one")[1]
        if concept[0] in vowel_sounds:
            if target[1] in vowel_sounds:
                return f"An {concept} can be an{target.replace(' or ', ', ')}, or another possibility, but only 1 at the same time. "
            else:
                return f"An {concept} can be a{target.replace(' or ', ', ')}, or another possibility, but only 1 at the same time. "
        else:
            if target[1] in vowel_sounds:
                return f"A {concept} can be an{target.replace(' or ', ', ')}, or another possibility, but only 1 at the same time. "
            else:
                return f"A {concept} can be a{target.replace(' or ', ', ')}, or another possibility, but only 1 at the same time. "
    else:
        target = rule.split("is one or multiple")[1]
        if concept[0] in vowel_sounds:
            if target[1] in vowel_sounds:
                return f"An {concept} can be an{target.replace(' or ', ', ')}, or another possibility, it can also be multiple at the same time. "
            else:
                return f"An {concept} can be a{target.replace(' or ', ', ')}, or another possibility, it can also be multiple at the same time. "
        else:
            if target[1] in vowel_sounds:
                return f"A {concept} can be an{target.replace(' or ', ', ')}, or another possibility, it can also be multiple at the same time. "
            else:
                return f"A {concept} can be a{target.replace(' or ', ', ')}, or another possibility, it can also be multiple at the same time. "


def parse_iden_tgt(rule):
    """
    Parse the identifier and targets from the given rule, and signal special cases

    Args:
        rule (str): The rule to be parsed.

    Returns:
        tuple: A tuple containing the identifier and target, or a tuple containing booleans to signal special rules.
    """

    result = ""
    if rule.startswith("Concept Type: "):
        return False, False

    elif rule.startswith("Necessity: ") or rule.startswith("Possibility: "):
        if "is of type" in rule:
            return True, rule.split("is of type ")[1]

        elif "is a component of" in rule:
            if rule.startswith("Necessity"):
                return "is a component of", rule.split("is a component of")[1]
            else:
                return "can be a component of", rule.split("is a component of")[1]

        elif "is composed of" in rule:
            if rule.startswith("Necessity"):
                return "is composed of", rule.split("is composed of")[1]
            else:
                return "can be composed of", rule.split("is composed of")[1]

        elif "is associated to" in rule:
            if rule.startswith("Necessity"):
                return "is associated to", rule.split("is associated to")[1]
            else:
                return "can be associated to", rule.split("is associated to")[1]

        elif "characterizes" in rule:
            if rule.startswith("Necessity"):
                return "characterizes", rule.split("characterizes")[1]
            else:
                return "can characterize", rule.split("characterizes")[1]

        elif "is characterized by" in rule:
            if rule.startswith("Necessity"):
                return "is characterized by", rule.split("is characterized by")[1]
            else:
                return "can be characterized by", rule.split("is characterized by")[1]

        elif "is a member of" in rule:
            if rule.startswith("Necessity"):
                return "is a member of", rule.split("is a member of")[1]
            else:
                return "can be a member of", rule.split("is a member of")[1]

        elif "has as members" in rule:
            if rule.startswith("Necessity"):
                return "has as members", rule.split("has as members")[1]
            else:
                return "can have as members", rule.split("has as members")[1]

        elif "is required by" in rule:
            if rule.startswith("Necessity"):
                return "is required by", rule.split("is required by")[1]
            else:
                return "can be required by", rule.split("is required by")[1]

        elif "requires" in rule:
            return "needs", rule.split("requires")[1]

        elif "is connected to" in rule:
            if rule.startswith("Necessity"):
                return "is connected to", rule.split("is connected to")[1]
            else:
                return "can be connected to", rule.split("is connected to")[1]

        elif "is" in rule:
            return True, True

    elif rule.startswith("General Concept: "):
        general = rule.split(":")[1].lstrip()
        if general[0] in vowel_sounds:
            return "is an ", general
        else:
            return "is a ", general

    return result


def order_groups(groups):
    """
   Order the groups such that groups sharing common elements are adjacent.

   Args:
       groups (list): List of groups to be ordered.

   Returns:
       list: Ordered list of groups.
   """

    ordered = [groups.pop(0)]

    while groups:
        last_group = ordered[-1]
        for i, group in enumerate(groups):
            if set(last_group) & set(group):
                ordered.append(groups.pop(i))
                break
        else:
            ordered.append(groups.pop(0))

    return ordered


def determine_groups(data):
    """
    Determine groups of concepts that lie close to each other in the given data.

    Args:
        data (dict): Dictionary containing concepts and their rules.

    Returns:
        list: List of groups of concepts.
    """

    groups = set()
    result = set()

    # Greate groups for all concepts mentioned in 1 sentence
    for concept, rules in data.items():
        for rule in rules:
            group = []
            for ref_concept in data.keys():
                if ref_concept != concept and re.search(rf'\b{re.escape(ref_concept)}\b', rule):
                    group.append(ref_concept)
            if len(group) > 0:
                group.append(concept)
                groups.add(tuple(sorted(group)))

    # Remove groups that are contained inside other groups
    for group_1 in groups.copy():
        contained = False
        for group_2 in groups.copy():
            if group_1 != group_2 and all(item in group_2 for item in group_1):
                contained = True
                break
        if not contained:
            result.add(tuple(sorted(group_1)))

    # Combine 3 groups if 3 groups combined cover only 3 concepts
    to_remove = set()
    to_add = set()
    for combo in combinations(result.copy(), 3):
        combined_values = set()
        for tpl in combo:
            combined_values.update(tpl)
        if len(combined_values) == 3:
            to_remove.update(combo)
            to_add.add(tuple(sorted(combined_values)))
    result.difference_update(to_remove)
    result.update(to_add)

    # Transform groups into a list is lists
    groups = result.copy()
    result = []
    for group in groups:
        grp = []
        for concept in group:
            grp.append(concept)
        result.append(grp)

    return result


def filter_data(rule, group, data):
    """
    Filter out rules that mention concepts not in the given group.

    Args:
        rule (str): The rule to be filtered.
        group (list): The group of concepts to check against.
        data (dict): Dictionary containing concepts and their rules.

    Returns:
        bool: True if the rule is relevant to the group, False otherwise.
    """

    for concept in data.keys():
        if concept not in group:
            if re.search(r'\b' + re.escape(concept) + r'\b', rule):
                return False
    return True


def write_paragraph(group, data):
    """
    Write a paragraph summarizing the relationships between concepts in a group.

    Args:
        group (list): List of concepts in the group.
        data (dict): Dictionary containing concepts and their rules.

    Returns:
        str: A paragraph summarizing the relationships between the concepts.
    """

    sentences_concept_identifier = []
    sentences_identifier_targets = []
    original_sentences = []
    target_specifics = defaultdict(list)
    result = "\n\n"

    for concept in group:
        for rule in data[concept]:
            if not filter_data(rule, group, data):
                continue

            identifier, target = parse_iden_tgt(rule)

            if isinstance(identifier, str):
                if concept[0] in vowel_sounds:
                    sentence = f"An {concept} {identifier}{target}. "
                else:
                    sentence = f"A {concept} {identifier}{target}. "
                sentences_concept_identifier.append((sentence, concept, identifier))
                sentences_identifier_targets.append((sentence, identifier, target))
                original_sentences.append(sentence)

            elif identifier:
                if isinstance(target, str):
                    target_specifics[target].append(concept)
                else:
                    result += handle_general(rule, concept)

    changed_1 = reduce_targets(sentences_concept_identifier)
    changed_2 = reduce_concepts(sentences_identifier_targets)

    covered = set()
    for sentence in original_sentences:
        if sentence in changed_1.keys():
            if changed_1[sentence] not in covered:
                covered.add(changed_1[sentence])
                result += changed_1[sentence]
        elif sentence in changed_2.keys():
            if changed_2[sentence] not in covered:
                covered.add(changed_2[sentence])
                result += changed_2[sentence]
        else:
            result += sentence

    return result
