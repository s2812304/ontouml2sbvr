"""
Module Name: sbvr2nl.py

This module is the file responsible for converting SBVR to NL.
"""

import os
from collections import defaultdict

from SBVR2NL import helper

data = defaultdict(list)


def main():
    """
    Translate all SBVR specifications into natural language.

    This function reads SBVR files from the input directory (Resources/SBVR), processes them to
    translate SBVR specifications into natural language, and writes the translated
    content to the output directory (Resources/NL).
    """

    print("Translating all SBVR specifications into natural language...")

    script_dir = os.path.dirname(os.path.abspath(__file__))
    input_path = os.path.join(script_dir, '../../Resources/SBVR')
    output_path = os.path.join(script_dir, '../../Resources/NL')

    # Loop over all files in the input directory
    for file_name in os.listdir(input_path):
        result = ""
        data.clear()

        # Read every input file
        with open(os.path.join(input_path, file_name), 'r') as file:
            concept_key = None
            concept_rules = []

            # Process line by line
            for line_ in file:
                line = line_.strip()
                # Skip empty lines
                if not line:
                    continue
                # Aggregate all rules for each concept
                if ":" not in line:
                    if concept_key is not None:
                        data[concept_key] = concept_rules
                    concept_key = line
                    concept_rules = []
                else:
                    concept_rules.append(line)
            data[concept_key] = concept_rules

            # Determine groups based on the SBVR specification
            groups = helper.determine_groups(data)

            # Order the groups and write a paragraph for each of them
            for group in helper.order_groups(groups):
                result += helper.write_paragraph(group, data)

        # Write the output to the output file
        with open(f"{output_path}/{file_name.split('.')[0]} in NL.txt", "w") as output_file:
            output_file.write(result)

    print("Translation done!")


if __name__ == "__main__":
    main()
