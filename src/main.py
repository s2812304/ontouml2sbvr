from OntoUML2SBVR import ontouml2sbvr
from SBVR2NL import sbvr2nl
from NL2Speech import nl2speech

if __name__ == "__main__":
    print("Running all transformations...")
    ontouml2sbvr.main()
    sbvr2nl.main()
    nl2speech.main()
    print("All transformations are now finished!")
