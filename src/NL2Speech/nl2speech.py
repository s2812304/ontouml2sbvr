"""
Module Name: nl2speech.py

This module is the file responsible for converting NL to speech.
"""

import os
from gtts import gTTS


def main():
    """
    Translate all natural language specifications into speech.

    This function reads natural language specifications from the input directory (Resources/NL), processes them to
    transform natural language specifications into speech, and puts the transformed
    content to the output directory (Resources/Speech).
    """

    print("Transforming all natural language specifications to MP3...\n"
          "This may take a while...")

    script_dir = os.path.dirname(os.path.abspath(__file__))
    input_path = os.path.join(script_dir, '../../Resources/NL')
    output_path = os.path.join(script_dir, '../../Resources/Speech')

    # Loop over all files in the input directory
    for file_name in os.listdir(input_path):
        # Read every input file
        with open(os.path.join(input_path, file_name), 'r') as file:
            text = file.read()

            # Increase the pause lengths for a clearer result
            text = text.strip("\n")
            text = text.replace(".", " ... ... ")
            text = text.replace("\n", " ... ... ... ... ")

            # Generate speech
            tts = gTTS(text=text, lang='en')

            # Safe the output to the output file
            tts.save(os.path.join(output_path, file_name.strip(" in NL.txt")) + ".mp3")

    print("Transformation done!")


if __name__ == "__main__":
    main()
