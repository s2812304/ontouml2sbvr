"""
Module Name: ontouml2sbvr.py

This module is the file responsible for converting OntoUML to SBVR.
"""

import os
from collections import defaultdict

from rdflib import Graph, URIRef

from OntoUML2SBVR import relationHelper, data_helper as h

data = defaultdict(list)
result = defaultdict(list)
helper = h.Helper(data)

# Lists with OntoUML stereotypes based on their SBVR concept type
roles = ["phase", "role", "phaseMixin", "roleMixin"]
general_concepts = ["kind", "subkind", "collective", "quantity", "relator", "category", "mixin", "mode", "quality"]

# Query for finding all classes
class_query = """
    SELECT ?s
    WHERE {
        ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://purl.org/ontouml-models/vocabulary/Class>.
    }
"""

# Query for finding all generalizations that are not part of a generalization set
generalization_query = """
    SELECT ?s
    WHERE {
        ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> 
        <https://purl.org/ontouml-models/vocabulary/Generalization> .
        FILTER NOT EXISTS {
            ?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> 
            <https://purl.org/ontouml-models/vocabulary/GeneralizationSet> .
            ?a ?p ?s .
        }
    }
"""

# Query for finding all generalization sets
gen_set_query = """
    SELECT ?s
    WHERE {
        ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> 
        <https://purl.org/ontouml-models/vocabulary/GeneralizationSet> .
    }
"""

# Query for finding all relations
relation_query = """
    SELECT ?s
    WHERE {
        ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://purl.org/ontouml-models/vocabulary/Relation> .
    }
"""

file_name_query = """
    SELECT ?o
    WHERE {
        ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://purl.org/ontouml-models/vocabulary/Project> .
        ?s <https://purl.org/ontouml-models/vocabulary/name> ?o
    }
"""


def handle_class(subject, statements):
    """
    Handle the processing of OntoUML classes.

    Args:
        subject: The subject in question.
        statements: The list of predicates and objects related to the subject.
    """

    concept_type = ""
    for pred, obj in statements:
        # Find out which concept type the class belongs to
        if pred == URIRef("https://purl.org/ontouml-models/vocabulary/stereotype"):
            if any(word in obj for word in roles):
                concept_type = "role"
            elif any(word in obj for word in general_concepts):
                concept_type = "general concept"

    # Define and add the rule connecting the class to its concept type
    result[subject].append(f"Concept Type:       {concept_type}")


def handle_generalization(statements):
    """
    Handle the processing of OntoUML generalizations.

    Args:
        statements: The list of predicates and objects related to a subject.
    """

    specific = ""
    general = ""

    # Find the specific and general
    for pred, obj in statements:
        if pred == URIRef("https://purl.org/ontouml-models/vocabulary/specific"):
            specific = obj
        elif pred == URIRef("https://purl.org/ontouml-models/vocabulary/general"):
            general = obj

    # Define and add the rule connecting the specific and general
    result[specific].append(f"General Concept:    {helper.get_name(general)}")


def handle_generalization_set(statements):
    """
    Handle the processing of OntoUML generalization sets.

    Args:
        statements: The list of predicates and objects related to a subject.
    """

    is_complete = False
    is_disjoint = False
    generalizations = []
    specifics = []

    # Find and declare the generalizations, is_complete and is_disjoint
    for pred, obj in statements:
        if pred == URIRef("https://purl.org/ontouml-models/vocabulary/isComplete"):
            if "true" in obj:
                is_complete = True
        elif pred == URIRef("https://purl.org/ontouml-models/vocabulary/isDisjoint"):
            if "true" in obj:
                is_disjoint = True
        elif pred == URIRef("https://purl.org/ontouml-models/vocabulary/generalization"):
            generalizations.append(obj)

    # Find the general concept of the set
    general = helper.get_general(generalizations[0])
    gen_name = helper.get_name(general)

    # Define and add a rule for each specific of the set
    for generalization in generalizations:
        specific = helper.get_specific(generalization)
        spec_name = helper.get_name(specific)
        result[specific].append(f"Necessity:          each {spec_name} is of type {gen_name}")
        specifics.append(specific)

    # Define a rule for the general based on is_complete and is_disjoint
    if is_complete:
        if is_disjoint:
            value = f"Necessity:          each {gen_name} is exactly one {helper.make_list(specifics)}"
        else:
            value = f"Necessity:          each {gen_name} is at least one {helper.make_list(specifics)}"
    else:
        if is_disjoint:
            value = f"Necessity:          each {gen_name} is at most one {helper.make_list(specifics)}"
        else:
            value = f"Possibility:        it is a possibility that {gen_name} is one or multiple {helper.make_list(specifics)}"

    # Add the rule for the general
    result[general].append(value)


def handle_relation(statements):
    """
    Handle the processing of OntoUML relations.

    Args:
        statements: The list of predicates and objects related to a subject.
    """

    relations = []
    stereotype = False
    inputs = relationHelper.get_source_and_target(statements, data)
    
    for pred, obj in statements:
        # Check if the relation has a stereotype
        if pred == URIRef("https://purl.org/ontouml-models/vocabulary/stereotype"):
            stereotype = True

            # Handle characterization relation
            if obj == URIRef("https://purl.org/ontouml-models/vocabulary/characterization"):
                relations.extend(relationHelper.handle_characterization(
                    inputs[0], inputs[1], inputs[2], inputs[3]))

            # Handle component of relation
            elif obj == URIRef("https://purl.org/ontouml-models/vocabulary/componentOf"):
                relations.extend(relationHelper.handle_component_of(
                    inputs[0], inputs[1], inputs[2], inputs[3]))

            # Handle member of relation
            elif obj == URIRef("https://purl.org/ontouml-models/vocabulary/memberOf"):
                relations.extend(relationHelper.handle_member_of(
                    inputs[0], inputs[1], inputs[2], inputs[3]))

            # Handle mediation relation
            elif obj == URIRef("https://purl.org/ontouml-models/vocabulary/mediation"):
                relations.extend(relationHelper.handle_mediation(
                    inputs[0], inputs[1], inputs[2], inputs[3]))

            # Handle material relation
            elif obj == URIRef("https://purl.org/ontouml-models/vocabulary/material"):
                relations.extend(relationHelper.handle_material(
                    inputs[0], inputs[1], inputs[2], inputs[3]))

    # Handle relations without a stereotype
    if not stereotype:
        inputs = relationHelper.get_source_and_target(statements, data, False)
        relations.extend(relationHelper.handle_relation(
                    inputs[0], inputs[1], inputs[2], inputs[3], inputs[4], inputs[5]))

    # Append relation to the result
    for (key, value) in relations:
        result[key].append(value)


def main():
    """
    Translate all OntoUML models into SBVR specifications.

    This function reads OntoUML files from the input directory (Resources/OntoUML), processes them to
    convert OntoUML models into SBVR specifications, and writes the converted
    content to the output directory (Resources/SBVR).
    """

    print("Translating all OntoUML models into SBVR specifications...")

    script_dir = os.path.dirname(os.path.abspath(__file__))
    input_path = os.path.join(script_dir, '../../Resources/OntoUML')
    output_path = os.path.join(script_dir, '../../Resources/SBVR')

    # Loop over all files in the input directory
    for file in os.listdir(input_path):
        result.clear()
        data.clear()

        # Parse the ontology from the input file
        g = Graph()
        g.parse(f"{input_path}/{file}", format="ttl")

        # Create a copy of the triples in dictionary format
        for s, p, o in g:
            data[s].append((p, o))

        # Initialize the helper class with the dictionary
        helper = h.Helper(data.copy())

        file_name = ""
        for name in g.query(file_name_query):
            file_name = name.o

        # Handle all class objects
        for subj in g.query(class_query):
            handle_class(subj.s, data.copy()[subj.s])

        # Handle all generalization objects
        for subj in g.query(generalization_query):
            handle_generalization(data.copy()[subj.s])

        # Handle all generalization set objects
        for subj in g.query(gen_set_query):
            handle_generalization_set(data.copy()[subj.s])

        # Handle all relation objects
        for subj in g.query(relation_query):
            handle_relation(data.copy()[subj.s])

        # Write the output to the output file
        with open(f"{output_path}/{file_name}.txt", "w") as output_file:
            for subj, stmts in result.items():
                output_file.write(f"\n{helper.get_name(subj)}\n")
                for stmt in stmts:
                    output_file.write(stmt + '\n')

    print("Translation done!")


if __name__ == "__main__":
    main()
