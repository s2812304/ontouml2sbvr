"""
Module Name: data_helper.py

This module contains a helper class with functions for to aid the ontouml2sbvr and relationHelper modules.
"""

from rdflib import URIRef


class Helper:
    """
    A helper class to facilitate the processing of OntoUML to SBVR transformations.

    Attributes:
        data (dict): A dictionary containing RDF triples.
    """

    def __init__(self, dataset):
        """
        Initializes the Helper class with the provided dataset.

        Args:
            dataset (dict): The dataset containing RDF triples.
        """

        self.data = dataset

    def get_general(self, key):
        """
        Retrieve the general concept for a given relation.

        Args:
            key: The relation to search for in the dataset

        Returns:
            The general concept URI or a not found message.
        """

        for pred, obj in self.data[key]:
            if pred == URIRef("https://purl.org/ontouml-models/vocabulary/general"):
                return obj

        return "GENERAL WAS NOT FOUND!"

    def get_specific(self, key):
        """
        Retrieve the specific concept for a given relation.

        Args:
            key: The relation to search for in the dataset

        Returns:
            The specific concept URI or a not found message.
        """

        for pred, obj in self.data[key]:
            if pred == URIRef("https://purl.org/ontouml-models/vocabulary/specific"):
                return obj

        return "SPECIFIC WAS NOT FOUND!"

    def get_name(self, key):
        """
        Retrieve the name of a concept for a given concept.

        Args:
            key: The concept to search for in the dataset

        Returns:
            The name of a concept or a not found message.
        """

        for pred, obj in self.data[key]:
            if pred == URIRef("https://purl.org/ontouml-models/vocabulary/name"):
                return str(obj)

        return "NAME WAS NOT FOUND!"

    def make_list(self, specifics):
        """
        Create a comma-separated list of names from the concepts in the specifics list.

        Args:
            specifics (list): A list of concepts.

        Returns:
            A string representing the comma-separated list of concept names from the specifics list.
        """

        result = ""
        for i, specific in enumerate(specifics):
            if i < len(specifics) - 2:
                result += f"{self.get_name(specific)}, "
            elif i == len(specifics) - 2:
                result += f"{self.get_name(specific)} or "
            else:
                return result + self.get_name(specific)
