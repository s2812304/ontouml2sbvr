"""
Module Name: relationHelper.py

This module contains various functions for the aid of the ontouml2sbvr module.
"""

from rdflib import URIRef

import OntoUML2SBVR.data_helper as h


helper = h.Helper(dict())


def init_helper(dataset):
    """
    Initialize the helper with the given dataset.

    Args:
        dataset (dict): The dataset containing RDF triples.
    """

    global helper
    helper = h.Helper(dataset)


def get_source_and_target(statements, data, stereotype=True):
    """
    Retrieve source and target information for a relation.

    Args:
        statements (list): The list of predicates and objects related to a subject.
        data (dict): The dataset containing RDF triples.
        stereotype (bool): Flag to indicate if a stereotype is used.

    Returns:
        list: A list containing source and target information.
    """

    init_helper(data)
    src = ""
    src_cardinality = ""
    src_agg_kind = ""
    tgt = ""
    tgt_cardinality = ""
    tgt_agg_kind = ""

    for pred, obj in statements:
        # Find out the information about the source end of a relation
        if pred == URIRef("https://purl.org/ontouml-models/vocabulary/sourceEnd"):
            for p, o in data[obj]:
                # Define the source object
                if p == URIRef("https://purl.org/ontouml-models/vocabulary/propertyType"):
                    src = o
                # Define the source cardinality
                elif p == URIRef("https://purl.org/ontouml-models/vocabulary/cardinality"):
                    src_cardinality = get_cardinality(data[o])
                # Define the source aggregation kind if there was no stereotype
                elif not stereotype and p == URIRef("https://purl.org/ontouml-models/vocabulary/aggregationKind"):
                    src_agg_kind = o

        # Find out the information about the target end of a relation
        elif pred == URIRef("https://purl.org/ontouml-models/vocabulary/targetEnd"):
            for p, o in data[obj]:
                # Define the target object
                if p == URIRef("https://purl.org/ontouml-models/vocabulary/propertyType"):
                    tgt = o
                # Define the target cardinality
                elif p == URIRef("https://purl.org/ontouml-models/vocabulary/cardinality"):
                    tgt_cardinality = get_cardinality(data[o])
                # Define the target aggregation kind if there was no stereotype
                elif not stereotype and p == URIRef("https://purl.org/ontouml-models/vocabulary/aggregationKind"):
                    tgt_agg_kind = o

    # Return the defined data
    if not stereotype:
        return [src, src_cardinality, src_agg_kind,
                tgt, tgt_cardinality, tgt_agg_kind]
    else:
        return [src, src_cardinality, tgt, tgt_cardinality]


def get_cardinality(statements):
    """
    Retrieve the cardinality from the given statements.

    Args:
        statements (list): The list of predicates and objects related to a subject.

    Returns:
        str: The cardinality as a string.
    """

    low = ""
    high = ""

    # Parse the low and high for the cardinality
    for pred, obj in statements:
        if pred == URIRef("https://purl.org/ontouml-models/vocabulary/lowerBound"):
            low = str(obj)
        elif pred == URIRef("https://purl.org/ontouml-models/vocabulary/upperBound"):
            high = str(obj)

    # Return a cardinality string based on low and high
    if low == "" and high == "":
        return f"any number of"
    elif low == high:
        return f"exactly {low}"
    elif low == "0":
        if high == "*":
            return False
        else:
            return f"at most {high}"
    else:
        if high == "*":
            return f"at least {low}"
        else:
            return f"at least {low} and at most {high}"


def handle_characterization(src, src_card, tgt, tgt_card):
    """
    Define and return rules for characterization relations.

    Args:
        src (str): Source concept.
        src_card (str): Source cardinality.
        tgt (str): Target concept.
        tgt_card (str): Target cardinality.

    Returns:
        list: A list of tuples containing the source and target rules.
    """

    src_name = helper.get_name(src)
    tgt_name = helper.get_name(tgt)
    if not src_card:
        src_string = f"Possibility:        it is possible that a {src_name} is characterized by some {tgt_name}"
    else:
        src_string = f"Necessity:          each {src_name} is characterized by {src_card} {tgt_name}"
    if not tgt_card:
        tgt_string = f"Possibility:        it is possible that a {tgt_name} characterizes some {src_name}"
    else:
        tgt_string = f"Necessity:          each {tgt_name} characterizes {tgt_card} {src_name}"

    return [(src, src_string), (tgt, tgt_string)]


def handle_component_of(src, src_card, tgt, tgt_card):
    """
    Define and return rules for component of relations.

    Args:
        src (str): Source concept.
        src_card (str): Source cardinality.
        tgt (str): Target concept.
        tgt_card (str): Target cardinality.

    Returns:
        list: A list of tuples containing the source and target rules.
    """

    src_name = helper.get_name(src)
    tgt_name = helper.get_name(tgt)
    if not src_card:
        src_string = f"Possibility:        it is possible that a {src_name} is composed of some {tgt_name}"
    else:
        src_string = f"Necessity:          each {src_name} is composed of {src_card} {tgt_name}"
    if not tgt_card:
        tgt_string = f"Possibility:        it is possible that a {tgt_name} is a component of some {src_name}"
    else:
        tgt_string = f"Necessity:          each {tgt_name} is a component of {tgt_card} {src_name}"

    return [(src, src_string), (tgt, tgt_string)]


def handle_member_of(src, src_card, tgt, tgt_card):
    """
    Define and return rules for member of relations.

    Args:
        src (str): Source concept.
        src_card (str): Source cardinality.
        tgt (str): Target concept.
        tgt_card (str): Target cardinality.

    Returns:
        list: A list of tuples containing the source and target rules.
    """

    src_name = helper.get_name(src)
    tgt_name = helper.get_name(tgt)
    if not src_card:
        src_string = f"Possibility:        it is possible that a {src_name} has as members some {tgt_name}"
    else:
        src_string = f"Necessity:          each {src_name} has as members {src_card} {tgt_name}"
    if not tgt_card:
        tgt_string = f"Possibility:        it is possible that a {tgt_name} is a member of some {src_name}"
    else:
        tgt_string = f"Necessity:          each {tgt_name} is a member of {tgt_card} {src_name}"

    return [(src, src_string), (tgt, tgt_string)]


def handle_mediation(src, src_card, tgt, tgt_card):
    """
    Define and return rules for mediation relations.

    Args:
        src (str): Source concept.
        src_card (str): Source cardinality.
        tgt (str): Target concept.
        tgt_card (str): Target cardinality.

    Returns:
        list: A list of tuples containing the source and target rules.
    """

    src_name = helper.get_name(src)
    tgt_name = helper.get_name(tgt)
    if not tgt_card:
        src_string = f"Possibility:        it is possible that a {src_name} requires some {tgt_name}"
    else:
        src_string = f"Necessity:          each {src_name} requires {tgt_card} {tgt_name}"
    if not src_card:
        tgt_string = f"Possibility:        it is possible that a {tgt_name} is required by some {src_name}"
    else:
        tgt_string = f"Necessity:          each {tgt_name} is required by {src_card} {src_name}"

    return [(src, src_string), (tgt, tgt_string)]


def handle_material(src, src_card, tgt, tgt_card):
    """
    Define and return rules for material relations.

    Args:
        src (str): Source concept.
        src_card (str): Source cardinality.
        tgt (str): Target concept.
        tgt_card (str): Target cardinality.

    Returns:
        list: A list of tuples containing the source and target rules.
    """

    src_name = helper.get_name(src)
    tgt_name = helper.get_name(tgt)
    if not tgt_card:
        src_string = f"Possibility:        it is possible that a {src_name} is connected to some {tgt_name}"
    else:
        src_string = f"Necessity:          each {src_name} is connected to {tgt_card} {tgt_name}"
    if not src_card:
        tgt_string = f"Possibility:        it is possible that a {tgt_name} is connected to some {src_name}"
    else:
        tgt_string = f"Necessity:          each {tgt_name} is connected to {src_card} {src_name}"

    return [(src, src_string), (tgt, tgt_string)]


def handle_association(src, src_card, tgt, tgt_card):
    """
    Define and return rules for relations with no stereotype.

    Args:
        src (str): Source concept.
        src_card (str): Source cardinality.
        tgt (str): Target concept.
        tgt_card (str): Target cardinality.

    Returns:
        list: A list of tuples containing the source and target rules.
    """

    src_name = helper.get_name(src)
    tgt_name = helper.get_name(tgt)
    if not tgt_card:
        src_string = f"Possibility:        it is possible that a {src_name} is associated to some {tgt_name}"
    else:
        src_string = f"Necessity:          each {src_name} is associated to {tgt_card} {tgt_name}"
    if not src_card:
        tgt_string = f"Possibility:        it is possible that a {tgt_name} is associated to some {src_name}"
    else:
        tgt_string = f"Necessity:          each {tgt_name} is associated to {src_card} {src_name}"

    return [(src, src_string), (tgt, tgt_string)]


def handle_relation(src, src_card, src_agg, tgt, tgt_card, tgt_agg):
    """
    Determine and handle the type of relation between two concepts based on aggregation kind.

    Depending on the aggregation kind of the source concept, either a component-of or an association
    relation is handled and returned.

    Args:
        src (str): Source concept.
        src_card (str): Source cardinality.
        src_agg (URIRef): Source aggregation kind
        tgt (str): Target concept.
        tgt_card (str): Target cardinality.
        tgt_agg (URIRef): Target aggregation kind

    Returns:
        list: A list of tuples containing the source and target rules.
    """

    if src_agg == URIRef("https://purl.org/ontouml-models/vocabulary/composite"):
        return handle_component_of(src, tgt_card, tgt, src_card)
    else:
        return handle_association(src, src_card, tgt, tgt_card)
