
F1 Season
Concept Type:       general concept
Necessity:          each F1 Season is composed of at least 8 Race

Race
Concept Type:       general concept
Necessity:          each Race is exactly one Regular Race or Sprint Race
Necessity:          each Race is a component of exactly 1 F1 Season
Necessity:          each Race requires exactly 1 Circuit
Possibility:        it is possible that a Race is required by some Driver
Necessity:          each Race requires exactly 1 Grand Prix
Necessity:          each Race requires exactly 1 F1 Car

Grand Prix
Concept Type:       general concept
Necessity:          each Grand Prix is required by at least 1 Race
Necessity:          each Grand Prix is connected to at least 1 Circuit

Driver
Concept Type:       role
Necessity:          each Driver is of type Racing Team Member
Necessity:          each Driver requires at least 2 Race
Necessity:          each Driver is connected to at least 1 F1 Car
Necessity:          each Driver is characterized by exactly 1 Driving Strategy

Racing Team
Concept Type:       general concept
Necessity:          each Racing Team has as members at least 2 Racing Team Member

Circuit
Concept Type:       general concept
Possibility:        it is possible that a Circuit is required by some Race
Possibility:        it is possible that a Circuit is connected to some Grand Prix

Engine
Concept Type:       general concept
Necessity:          each Engine is of type F1 Car Piece
Necessity:          each Engine is a component of exactly 1 F1 Car

Tire
Concept Type:       general concept
Necessity:          each Tire is of type F1 Car Piece
Necessity:          each Tire is a component of exactly 1 F1 Car

Chassis
Concept Type:       general concept
Necessity:          each Chassis is of type F1 Car Piece
Necessity:          each Chassis is a component of exactly 1 F1 Car

F1 Car
Concept Type:       general concept
Necessity:          each F1 Car is composed of exactly 4 Tire
Necessity:          each F1 Car is composed of exactly 1 Engine
Necessity:          each F1 Car is composed of exactly 1 Chassis
Possibility:        it is possible that a F1 Car is required by some Race
Possibility:        it is possible that a F1 Car is connected to some Driver

Person
Concept Type:       general concept

F1 Car Piece
Concept Type:       general concept
Necessity:          each F1 Car Piece is at most one Tire, Chassis or Engine

Engineer
Concept Type:       role
Necessity:          each Engineer is of type Racing Team Member

Mechanic
Concept Type:       role
Necessity:          each Mechanic is of type Racing Team Member

Racing Team Member
Concept Type:       role
General Concept:    Person
Necessity:          each Racing Team Member is at most one Driver, Team Manager, Engineer or Mechanic
Necessity:          each Racing Team Member is a member of exactly 1 Racing Team

Team Manager
Concept Type:       role
Necessity:          each Team Manager is of type Racing Team Member

Regular Race
Concept Type:       general concept
Necessity:          each Regular Race is of type Race

Sprint Race
Concept Type:       general concept
Necessity:          each Sprint Race is of type Race

Championship Winner Driver
Concept Type:       role
General Concept:    Driver
Necessity:          each Championship Winner Driver is of type Champion

Championship Winner Constructor
Concept Type:       role
General Concept:    Racing Team
Necessity:          each Championship Winner Constructor is of type Champion

Champion
Concept Type:       role
Necessity:          each Champion is exactly one Championship Winner Constructor or Championship Winner Driver
Necessity:          each Champion is exactly one Past Champion or Current Champion

Current Champion
Concept Type:       role
Necessity:          each Current Champion is of type Champion

Past Champion
Concept Type:       role
Necessity:          each Past Champion is of type Champion

Driving Strategy
Concept Type:       general concept
Necessity:          each Driving Strategy is exactly one Aggressive Strategy or Conservative Strategy
Necessity:          each Driving Strategy characterizes exactly 1 Driver

Aggressive Strategy
Concept Type:       role
Necessity:          each Aggressive Strategy is of type Driving Strategy

Conservative Strategy
Concept Type:       role
Necessity:          each Conservative Strategy is of type Driving Strategy
