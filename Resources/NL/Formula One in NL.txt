

A Driver needs at least 2 Race. A Driver is connected to at least 1 F1 Car. A F1 Car can be required by some Race. A F1 Car can be connected to some Driver. A Race can be required by some Driver. A Race needs exactly 1 F1 Car. 

A Circuit can be required by some Race. A Circuit can be connected to some Grand Prix. A Grand Prix is required by at least 1 Race. A Grand Prix is connected to at least 1 Circuit. A Race needs exactly 1 Circuit, and exactly 1 Grand Prix. 

Every Race is either a Regular Race or Sprint Race. 

A F1 Season is composed of at least 8 Race. A Race is a component of exactly 1 F1 Season. 

A F1 Car Piece can be a Tire, Chassis, Engine, or another possibility, but only 1 at the same time. Every Chassis, Engine, or Tire is a component of exactly 1 F1 Car. A F1 Car is composed of exactly 4 Tire, exactly 1 Engine, and exactly 1 Chassis. 

A Racing Team Member is a Person. 

A Racing Team Member can be a Driver, Team Manager, Engineer, Mechanic, or another possibility, but only 1 at the same time. A Racing Team has as members at least 2 Racing Team Member. A Racing Team Member is a member of exactly 1 Racing Team. 

A Driver is characterized by exactly 1 Driving Strategy. A Driving Strategy characterizes exactly 1 Driver. 

Every Driving Strategy is either an Aggressive Strategy or Conservative Strategy. 

Every Champion is either a Past Champion or Current Champion. 

Every Champion is either a Championship Winner Constructor or Championship Winner Driver. A Championship Winner Driver is a Driver. 

A Championship Winner Constructor is a Racing Team. 